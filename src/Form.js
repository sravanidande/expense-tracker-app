import React from 'react'
import { Form, FormGroup, Label ,Input,Col, Button} from 'reactstrap'


export const AddExpenseForm = ({onCreateExpense,onDelete}) => {
    const [name,setName] = React.useState('')
    const [amount,setAmount] = React.useState('')
  
    const handleName = (evt) => {
      setName(evt.target.value)
    }
  
    const handleAmount = (evt) =>{
      setAmount(evt.target.value)
    }
    const handleSubmitForm = evt => {
        evt.preventDefault()
        onCreateExpense(name,amount)
        setName('')
        setAmount('')
    }
    return (
        <Form style={{margin:10}} onSubmit={handleSubmitForm}>
            <FormGroup className="row">
                <Label sm={2}>Name of Expense</Label>
                <Col sm={4}>
                <Input type="text" name="name" value={name} onChange={handleName} placeholder="Name of Expense"/>
                </Col>
            </FormGroup>
            <FormGroup className="row">
                <Label sm={2}>$ Amount</Label>
                <Col sm={4}>
                <Input type="number" name="amount" value={amount} onChange={handleAmount} placeholder="0.00"/>
                </Col>
            </FormGroup>
            <Button type="submit" color="success">Add</Button>
            <Button type="submit" color="danger" onClick={onDelete}>Delete</Button>
        </Form>
    )
}