import React from 'react'
import { ListGroup, ListGroupItem } from 'reactstrap'

export const ExpenseList = ({expenses}) => {
    return (
        <ListGroup>
            {expenses.map(ex=>
            <ListGroupItem key={ex.name}>
                {ex.name} - $ {ex.amount}
            </ListGroupItem>
            )}
        </ListGroup>
    )
}