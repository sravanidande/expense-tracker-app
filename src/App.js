import React from 'react'
import { Container, Jumbotron } from 'reactstrap'
import { AddExpenseForm } from './Form'
import { ExpenseList } from './List'
import Logo from './logo.svg'

const ALL_EXPENSES = localStorage.getItem('expenses') ? JSON.parse(localStorage.getItem('expenses')) : []

function App() {
  const [expenses,setExpenses] = React.useState(ALL_EXPENSES)
  const handleCreateExpense = (name,amount) => {
    if(name!==''&& amount>0){
      const expense = {name,amount}
      setExpenses([...expenses,expense])
    }else{
      console.log('Invalid expense name or the amount')
    }
  }
  React.useEffect(()=>{
    localStorage.setItem('expenses',JSON.stringify(expenses))
  },[expenses])

  const handleDelete = () => {
    localStorage.clear()
    setExpenses([])
  }

  return (
    <Container className="text-center">
      <Jumbotron fluid>
        <h3 className="display-6">
          Expense Tracker React App<br/>
          <img src={Logo} style={{ width: 50, height: 50 }} alt="react-logo" />
        </h3>
        <p>
          Total Expense:{''}
          <span className="text-success">
            $
            {expenses.reduce((acc,v)=>(acc+parseInt(v.amount)),0)}
          </span>
        </p>
      <AddExpenseForm onCreateExpense={handleCreateExpense} onDelete={handleDelete}/>
      <ExpenseList expenses={expenses}/>
      </Jumbotron>
    </Container>
  )
}

export default App;

// const [count,setCount] = React.useState(0)
//   return (
//     <Container style={{margin:20}}>
//       <p className="text-primary">you click {count} times</p>
//       <Button color="success" onClick={()=>setCount(count+1)}>increase count</Button>
//       <Button color="danger" onClick={()=>setCount(count-1)}>decrease count</Button>
//     </Container>
//   );